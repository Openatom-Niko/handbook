# <center>开源项目B站直播一纸禅
### 前期宣传
| 渠道 |内容|
|:-----------:|:---|
|__自由渠道__|1、	官方网站：提前一周在官网以banner、新闻、活动公告等形式发布；<br>2、	微信公众号：直播当天推送；<br>3、	B站动态板块：直播当天推送；<br>4、	微信交流群：群内提前发海报（有预算可引导用户，转发朋友圈送书等形式进行）；<br>5、	其余宣传：小助手、工作人员、讲师转发朋友圈。
|__外部渠道__|1、	技术社区、论坛：OSCChina、CSDN、infoQ、华为开源公众号、华为开发者公众号等；在各个社区注册官方账号发帖宣传，可考虑通过发布技术干货文章，附带直播推广的形式进行；如有预算可以投放广告、置顶帖子、文章推荐等形式进行；<br>2、	微信公众号：有预算的情况下，可选择一些与直播主题强相关的微信公众号进行投放，如机器之心；<br>3、	外部合作的微信群：友好合作的技术社区的群；商业合作伙伴的群等；<br>4、	往期参与的活动沉淀的微信群、QQ群等。

### 直播前
| 人员 |内容|
|:------:|:---|
|__讲师__|__PPT制作注意：__<br>1、	预留右上角头像空间；<br>2、	可放一页宣传PPT提示开始时间、展示本期竞猜预告、公众号及群二维码等宣传信息，或是一些其他引起观众对本场直播有所期待的内容。<br>3、	每一部分可以预留一页总结，或者根据该部分所设计的问题，用于提醒互动，引导用户光柱使用。<br>4、	PPT页面尽可能的加入多的动态效果，并保证画面呈现的每个细节都是可以被清楚地看到的。<br>5、	不要过多的文字堆砌，多些示意图和图片。<br>6、	PPT所呈现地每个知识点都必须可以用简单语言陈述，如无法陈述清晰则不要出现在PPT上。<br>__直播前准备：__<br>1、	提前录制好备用回放视频，出现直播事故，可以引导学生观看已经录制好的视频；<br>2、	收集好暖场视频；<br>3、	与小助手沟通好需要弹幕提醒的文字协助的内容，每条弹幕只能写20个字；<br>4、	提前一天将准备好的PPT、课前准备、作业、备用视频发给小助手；<br>5、	提前一天调试直播的画面和网络，如需露脸，需自行准备好灯光，背景应选择干净整洁的，并测试好人物画面角度。
|__小助手__|1、	设计直播海报、直播封面（≥960*600；＜2M）。<br>2、	收集好PPT、课前准备、作业、备用视频，准备直播后使用。<br>3、	提前一天将课前准备发布到群公告。<br>4、	将备用视频和作业上传B站，定时直播后半小时发布（直播结束后，可将视频撤下，更换为直播录屏）。<br>5、	直播预告和群提醒：提前一天在朋友圈宣传，开播前30分钟提醒，开播前5分钟提醒，开播时提醒带直播链接，并同步到朋友圈。<br>6、	提前准备好活跃弹幕，B站弹幕限制字数为20字。<br>7、	与讲师沟通好整个直播流程，包括需要协助的内容，如弹幕文字提醒准备、收集问题发布时间节点等。

### 直播中
| 人员 |内容|
|:------:|:---|
|__讲师__|1、	建议在有小助手现场协助直播，如遇到问题可以寻求场外协助。<br>2、	直播打开弹幕，手机调静音，但需要留意工作群信息。<br>3、	记得点击录制视频。<br>4、	提前30分钟-1小时开启直播间，播放暖场视频或音乐。<br>5、	开播前10分钟可见画面调整为PPT的宣传页面，提示开始时间、展示本期竞猜预告、公众号及群二维码等宣传信息，或是一些其他引起观众对本场直播有所期待的内容。<br>6、	可以设置一些互动小细节：如播放歌曲，支持粉丝点歌，和粉丝闲聊（如要讲话，需将视频或音乐的声音调小）。<br>7、	讲解内容最好少于45分钟，每部分可以设置1个新的问题或者是有个小结，与用户进行互动，此部分可以考虑送小礼品，如前5个抢答正确的用户可获得MindSpore官方书籍等形式进行。<br>8、	讲解过程中可以尽量挖掘或引用一些小故事、案例，让直播内容更生动。<br>9、	可在每个节点结束后，询问用户是否听明白，明白请在弹幕扣1，并选择1、2个问题进行回答，其余问题可以留在后面的答疑环节，或者在微信群解答。<br>10、	可以在直播中不时的引导用户在弹幕提问，有奖提问，不要一味光顾讲课。<br>11、	引导用户加小助手微信号，进官方微信交流群。<br>12、	遇到状况，可寻求场外协助，继续保持和用户进行互动，由小助手协助解决。解决状况的同时，不需要反复和用户确认是否已经解决了，可以通过小助手，或者微信群了解，务必时刻保持和用户互动，不要冷场，避免用户流失。
|__小助手__|1、	发布提前准备好的欢迎弹幕，欢迎用户进入直播间，引导用户和讲师进行交流。<br>2、	直播期间协助讲师，将相关信息文字同步发布在弹幕上。<br>3、	协同工作人员多个账号在弹幕活跃气氛，将准备好的弹幕话语、问题充当普通观众发弹幕，为其他观众营造出积极参与并提问的氛围，持续抓住观众对直播间的注意力，并促进互动。<br>4、	时刻做好问题收集，一直持续到直播结束。在讲解即将结束时，将部分问题整合发到工作群，讲师进行答疑。剩余问题可以留到之后的答疑使用。<br>5、	遇到状况，不要慌乱，引导老师继续保持和用户互动。进行问题解决。<br>6、	如问题无法解决，可引导用户前往B站观看备用视频，并将PPT发布到微信学习群。

### 直播后
| 人员 |内容|
|:------:|:---|
|__讲师__|1、	及时将直播录屏、作业等资料发送给小助手。<br>2、	积极留意群信息进行答疑，或者留意小助手的提醒信息。如工作较忙可以定一个固定的时间块，由小助手整理好问题，在统一的时间段进行答疑。<br>3、	如时间允许，可输出一篇相应的技术文章，用在内外部各个传播，沉淀直播内容，扩大内容影响力。
|__小助手__|1、	直播结束第一时间将课堂反馈发到各个学习群公告。<br>2、	将视频剪辑好发布B站（封面加入SWF的动效），审核完毕将视频及本次直播的资料分享到微信学习群。（考虑将相关的资料上传百度网盘，发布在公众号，引导用户关注公众号）。<br>3、	将直播中的礼品发放信息收集好。<br>4、	引导讲师对微信群的问题进行答疑。<br>5、	如时间允许，可将讲师生成的技术文章发布到公众号，形成直播回顾。

### 直播新形式
可采用多人出镜，以沟通交流的方式去呈现某个主题内容，此方式需提前策划好脚本。如两位讲师不在一个地方，可采用腾讯会议或welink会议推流到B站的模式实现。
